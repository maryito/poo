from Veterinaria import Veterinaria

if __name__ == '__main__':
    print("POO con Python3 ")

    # Creando una instancia
    myHouse = Veterinaria()

    # Asignando valores a los atributos
    # myHouse.nombre = input("Ingrese el nombre: ")
    # myHouse.direccion = input("Direccion: ")
    # myHouse.telefono = "200-9980"
    # myHouse.estado = True

    # Acceder a los atributo
    print(myHouse.nombre)

    # myHouse.nombre = "My nombre desde la instancia"
    # myHouse.crear()

    empresDog =  Veterinaria('Empresa Dog', 'Costa del Este', '2000009')
    empresCat =  Veterinaria('Empresa Cat', 'Albrook', '8981291821')
    empresGeneral = Veterinaria('Empresa General', 'Tumba Muerto', '19281291')


    empresGeneral.consultar()

